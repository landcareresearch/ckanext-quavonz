# ckanext-quavonz changelog

## 2022-09-08 Version 0.2.10

- Fixed user search plugin.

## 2021-08-11 Version 0.2.9

- Added new entries to Sample Collection Method, Depositional Environment, and Current Site Vegetation.

## 2021-07-16 Version 0.2.8

- Replaced key values for Island names.
- Added more special characters for site labs and site name fields.
- Updated the Collectors and Analytics fields.
- Added a value in data sources field.
- Added a new entry for data sources and replaced a title for charcoal.

## 2021-07-05 Version 0.2.7

- Fixed a double entry for North Island for the islands field.

## 2021-06-09 Version 0.2.6

- Disabled Browser caching on input fields.

## 2021-05-19 Version 0.2.5

- Added a default value for status fields which forces 'incomplete' when new datasets are created.

## 2021-05-12 Version 0.2.4

- Changed the publications field to use multi text.

## 2021-05-12 Version 0.2.3

- Added new fields metadata and raw data source status.
- Changed 2 text fields into text boxes with markdown support.
- Set the dataset description to the site_description viable in the search list for datasets.

## 2021-05-11 Version 0.2.2

- Added a PDF that contains the site descriptions .
- Extended the default new dataset documentation with specific QUAVONZ definition and link to pdf.

## 2021-05-11 Version 0.2.1

- Changed the web site font.
- Fixed colored text on all pages that were not home page.

## 2021-05-10 Version 0.2.0

- Add new collectors and analysts.
- Changed the Site Lab ID tag.
- Updated the Islands field.
- Updated the Field Descriptions.
- Updated the CSS for site styling.

## 2021-04-19 Version 0.1.15

- Changed the validation and formatting on the Title.

## 2021-04-12 Version 0.1.14

- Changed Collectors and Analysts fields to use pre-populated checkboxes.

## 2021-04-08 Version 0.1.13

- Populated Select choices from spreadsheet (Islands have 900+ entries).
- Changed some Selects to MultiSelect Checkboxes.

## 2021-04-07 Version 0.1.12

- Added tags for the Site Name Field.
- Auto sets both the data slug and title based on the site name and site id fields.

## 2021-03-31 Version 0.1.11

- Added a new option to the Data Source category.
- Updated an option of the Data Source category.

## 2021-03-31 Version 0.1.10

- Fixed the ordering of the lat and lon coordiantes (where mixed up).

## 2021-03-31 Version 0.1.9

- Added the Dataset Extent Map.

## 2021-03-31 Version 0.1.8

- Fixed search box on front page (changed text color).
- Fixed capitalization of fields in schema.

## 2021-03-30 Version 0.1.7

- Added additional extra fields to schema.
- Updated extra fields with form placeholder text.

## 2021-03-29 Version 0.1.6

- Added spatial validation which auto creates a bounding box.
  Creates a 50m area bounding box based on a point.

## 2021-03-25 Version 0.1.5

- Reverted the login page to use the default login page which uses the ckanext-ldap plugin.
- Added a changelog (including previous versions).

## 2021-03-24 Version 0.1.4

- Added validators for numbers (positive and negative floats).
- Fixed issues with empty inputs for year and number validators.

## 2021-03-22 Version 0.1.3

- Completed additional selects with values from the example.

## 2021-03-22 Version 0.1.2

- Moved the schema to a more appropriate directory.
- Updated schema with additional fields.

## 2021-03-18 Version 0.1.1

- Fixed an issue where pulldowns were not setting values when editing metadata.

## 2021-03-18 Version 0.1.0

- Initial Release.
