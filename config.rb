module MyVars
    HOSTNAME               = "dev-metadata-quavonz"
    OS_FILE                = "ubuntu18.04"
    MEMORY                 = "4096"
    CPUS                   = "1"
    IP                     = "192.168.33.25"
    GENCACHE               = false
    PROJECT_PATH           = "../"
    FOLDERS                = {
        "ckanext-quavonz" => {
            "src"  => "../.",
            "dest" => "/opt/ckanext-quavonz"
        }
    }
end
