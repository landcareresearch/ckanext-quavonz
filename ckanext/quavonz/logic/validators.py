import sys
import datetime 
import math
  
from ckan.plugins.toolkit import Invalid

def is_number(value):
    '''Validates value is a number'''
    if value is None:
        return None
    if hasattr(value, 'strip') and not value.strip():
        return None

    try :
        float(value)
    except ValueError :
        raise Invalid("The value is not a number")

    return value

def is_elevation_range(value):
    '''Validates value is within a valid range'''
    if value is None:
        return None
    if hasattr(value, 'strip') and not value.strip():
        return None

    try :
        r = float(value)
        if -20 < r and r < 3000 :
            return value
        else :
            raise Invalid("The value is not a number between -20 and 3000")
    except ValueError :
        raise Invalid("The value is not a number between -20 and 3000")

    return value

def is_year(value):
    if value is None:
        return None
    if hasattr(value, 'strip') and not value.strip():
        return None
    if value is '':
        return None
    '''Validates value is the date format YYYY'''
    try:
        datetime.datetime.strptime(value, '%Y')
    except ValueError:
        raise Invalid("Year must be in the format YYYY, e.g. 2015")
    return value

def is_year_month(value):
    '''Validates value is in the date format YYYY-MM'''
    try:
        datetime.datetime.strptime(value, '%Y-%m')
    except ValueError:
        raise Invalid("Date must be in the format YYYY-MM, e.g. 2015-01")
    return value

def is_year_month_day(value):
    '''Validates value is in the date format YYYY-MM-DD'''
    try:
        datetime.datetime.strptime(value, '%Y-%m-%d')
    except ValueError:
        raise Invalid("Date must be in the format YYYY-MM-DD, e.g. 2015-01-29")
    return value

def is_date(value):
    '''Validates value is in one of three various formats'''
    for m in ['is_year', 'is_year_month', 'is_year_month_day']:
        try:
            getattr(sys.modules[__name__], m)(value)
        except Invalid:
            pass
        else:
            return value

    raise Invalid("Date must be in the format YYYY-MM-DD, YYYY-MM, or YYYY")

# === for creating a bounding box around a lat/lon === #
# Semi-axes of WGS-84 geoidal reference
WGS84_a = 6378137.0  # Major semiaxis [m]
WGS84_b = 6356752.3  # Minor semiaxis [m]

# Earth radius at a given latitude, according to the WGS-84 ellipsoid [m]
def WGS84EarthRadius(lat):
    # http://en.wikipedia.org/wiki/Earth_radius
    An = WGS84_a*WGS84_a * math.cos(lat)
    Bn = WGS84_b*WGS84_b * math.sin(lat)
    Ad = WGS84_a * math.cos(lat)
    Bd = WGS84_b * math.sin(lat)
    return math.sqrt( (An*An + Bn*Bn)/(Ad*Ad + Bd*Bd) )

# Bounding box surrounding the point at given coordinates,
# assuming local approximation of Earth surface as a sphere
# of radius given by WGS84
def boundingBox(lat, lon, halfSideInKm):
    halfSide = 1000*halfSideInKm

    # Radius of Earth at given latitude
    radius = WGS84EarthRadius(lat)
    # Radius of the parallel at given latitude
    pradius = radius*math.cos(lat)

    latMin = lat - halfSide/radius
    latMax = lat + halfSide/radius
    lonMin = lon - halfSide/pradius
    lonMax = lon + halfSide/pradius

    return (latMin, lonMin, latMax, lonMax)

def convert_spatial(key, data, errors, context):

    #print data
    #('spatial',): u'{ "type": "Polygon","coordinates": [ [ [175.929863839, -37.0334631607],[175.929848161, -37.0334631607],[175.929848161, -37.0334788393], [175.929863839, -37.0334788393], [175.929863839, -37.0334631607] ] ] }', 

    # manage empty lat and long fields.
    if data is None:
        return None
    if hasattr(data , 'strip') and not value.strip():
        return None
    if data.get(('latitude',)) is None:
        data[('spatial',)] = ''
        return None
    if data.get(('longitude',)) is None:
        data[('spatial',)] = ''
        return None

    try:
      # get coordinates
      lat = float(data.get(('latitude',)))
      lon = float(data.get(('longitude',)))
    except ValueError:
        raise Invalid("Latitude and Longitude must be in radians")

    #print 'lat = ' , lat
    #print 'lon = ' , lon

    # Currently using a 50m area
    bbox = boundingBox(lat,lon,0.05)
    #latMin = bbox[0]
    #latMax = bbox[2]
    #lonMin = bbox[1]
    #lonMax = bbox[3]

    # store coordinates as lon / lat
    #upper_left  = (str(bbox[2]),str(bbox[3]))
    #upper_right = (str(bbox[0]),str(bbox[3]))
    #lower_right = (str(bbox[0]),str(bbox[1]))
    #lower_left  = (str(bbox[2]),str(bbox[1]))

    upper_left  = (str(bbox[3]),str(bbox[2]))
    upper_right = (str(bbox[3]),str(bbox[0]))
    lower_right = (str(bbox[1]),str(bbox[0]))
    lower_left  = (str(bbox[1]),str(bbox[2]))

    #print 'upper_left = ' , upper_left
    #print 'upper_right = ' , upper_right
    #print 'lower_right = ' , lower_right
    #print 'lower_left = ' , lower_left

    #[175.929856, -37.033471],      [0,0]
    #[175.958352, -37.033471],      [1,0]
    #[175.958352, -37.063339],      [1,1]
    #[175.929856, -37.063339],      [0,1]
    #[175.929856, -37.033471]       [0,0]

    polygon = "{ \"type\": \"Polygon\",\"coordinates\": [ [ [" + upper_left[0] + ", " + upper_left[1] + "],[" + upper_right[0] + ", " + upper_right[1] + "],[" + lower_right[0] + ", " + lower_right[1] + "], [" + lower_left[0] + ", " + lower_left[1] + "], ["+ upper_left[0] + ", " + upper_left[1] + "] ] ] }"

    # set the spatial value automatically
    data[('spatial',)] = polygon

def fix_string(name):
    # iterate through replacing commas with hyphens if present.
    name_combo = ''
    for ch in name :
        if ch == ' ' or ch == '/' :
          name_combo += '-'
        elif ch == "," :
          name_combo += '_'
        elif ch == "'" :
          name_combo += ''
        else :
          name_combo += ch
    return name_combo

def convert_title(key, data, errors, context):

    # manage empty lat and long fields.
    if data is None:
        return None
    if hasattr(data , 'strip') and not value.strip():
        return None
    if data.get(('site_lab_id',)) is None:
        raise Invalid("Site Lab ID is required")
    if data.get(('site_name',)) is None:
        raise Invalid("Site Name is required")
    
    site_lab_id = data[('site_lab_id',)]
    site_name   = data[('site_name',)]

    # iterate through replacing commas with hyphens if present.
    site_lab_fixed  = fix_string(site_lab_id)
    site_name_fixed = fix_string(site_name)

    site_title = site_lab_fixed + '_' + site_name_fixed

    data[('title',)] = site_title
    data[('name',)] = site_title.lower()

