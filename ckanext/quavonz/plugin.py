import json

from ckan import plugins
from ckan.plugins import toolkit
from flask import Blueprint

import ckanext.quavonz.logic.validators as validators

def terms_of_use():
    return toolkit.render('terms_of_use.html')

class QuavonzPlugin(plugins.SingletonPlugin, toolkit.DefaultDatasetForm):
    plugins.implements(plugins.IBlueprint)
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IDatasetForm)
    plugins.implements(plugins.IValidators)
    plugins.implements(plugins.IPackageController, inherit=True)
    plugins.implements(plugins.IFacets)
    plugins.implements(plugins.ITemplateHelpers)
    def get_helpers(self):
        return {'quavonz_country_codes': quavonz_country_codes}

    def _modify_package_schema(self, schema):
        schema.update({
            'site_lab_id'     : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_converter('convert_to_extras')],
            'site_name'       : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_converter('convert_to_extras')],
            'island'          : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_converter('convert_to_extras')],
            'geopolitical'    : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_converter('convert_to_extras')],
            'location_lat'    : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_converter('convert_to_extras')],
            'location_long'   : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_converter('convert_to_extras')],
            'collection_date' : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_validator('ckanext_quavonz_is_year'),
                                 toolkit.get_converter('convert_to_extras')],
            'doi': [toolkit.get_validator('ignore_empty'),
                    toolkit.get_converter('convert_to_extras')],
        })
        schema['author'] = [toolkit.get_validator('repeating_text'),
                            toolkit.get_validator('ignore_empty')]
        return schema

    def create_package_schema(self):
        schema = super(QuavonzPlugin, self).create_package_schema()
        return self._modify_package_schema(schema)

    def update_package_schema(self):
        schema = super(QuavonzPlugin, self).update_package_schema()
        return self._modify_package_schema(schema)

    def show_package_schema(self):
        schema = super(QuavonzPlugin, self).show_package_schema()
        schema.update({
            'site_lab_id'     : [toolkit.get_converter('convert_from_extras'),
                                 toolkit.get_validator('ignore_empty')],
            'site_name'       : [toolkit.get_converter('convert_from_extras'),
                                 toolkit.get_validator('ignore_empty')],
            'island'          : [toolkit.get_converter('convert_from_extras'),
                                 toolkit.get_validator('ignore_empty')],
            'geopolitical'    : [toolkit.get_converter('convert_from_extras'),
                                 toolkit.get_validator('ignore_empty')],
            'location_lat'    : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_converter('convert_to_extras')],
            'location_long'   : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_converter('convert_to_extras')],
            'collection_date' : [toolkit.get_validator('ignore_empty'),
                                 toolkit.get_validator('ckanext_quavonz_is_year'),
                                 toolkit.get_converter('convert_to_extras')],
            'doi'             : [toolkit.get_converter('convert_from_extras'),
                                 toolkit.get_validator('ignore_empty')],
        })
        schema['author'] = [toolkit.get_validator('repeating_text_output'),
                            toolkit.get_validator('ignore_empty')]
        return schema

    def is_fallback(self):
        return True

    def package_types(self):
        return []

    def get_blueprint(self):
        blueprint = Blueprint('quavonz', self.__module__)
        rules = [
            ('/terms_of_use','terms_of_use',terms_of_use),
        ]
        for rule in rules:
            blueprint.add_url_rule(*rule)
        return blueprint

    def update_config(self, config):
        # from the IConfigurer interface, we're telling ckan
        # where our templates are kept in this pluign
        toolkit.add_template_directory(config, 'templates')
        # add our extension's public directory, to include the custom css file
        toolkit.add_public_directory(config, 'public')
        toolkit.add_resource('assets', 'quavonz')

    def get_validators(self):
        return {
            'ckanext_quavonz_is_year'            : validators.is_year,
            'ckanext_quavonz_is_date'            : validators.is_date,
            'ckanext_quavonz_is_number'          : validators.is_number,
            'ckanext_quavonz_is_elevation_range' : validators.is_elevation_range,
            'ckanext_quavonz_convert_spatial'    : validators.convert_spatial,
            'ckanext_quavonz_convert_title'      : validators.convert_title
        }

    def before_index(self, dataset_dict):
        '''
        Insert `vocab_authors` into solr index with list of authors derived
        from the dataset_dict's `author` field.
        '''
        def listify_author(author_value):
            if isinstance(author_value, list):
                return author_value
            if author_value is None:
                return []
            try:
                return json.loads(author_value)
            except ValueError:
                return [author_value]

        author_value = listify_author(dataset_dict.get('author'))

        if dataset_dict.get('author'):
            dataset_dict['vocab_author'] = author_value

        return dataset_dict

    # IFacets

    def dataset_facets(self, facets_dict, package_type):
        _update_facets(facets_dict)
        return facets_dict

    def group_facets(self, facets_dict, group_type, package_type):
        _update_facets(facets_dict)
        return facets_dict

    def organization_facets(self, facets_dict, organization_type, package_type):
        _update_facets(facets_dict)
        return facets_dict

def _update_facets(facets_dict):

    facets_dict.update({
        'vocab_author': plugins.toolkit._('Authors')
    })

def create_country_codes():
    user = toolkit.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'country_codes'}
        toolkit.get_action('vocabulary_show')(context, data)
    except toolkit.ObjectNotFound:
        data = {'name': 'country_codes'}
        vocab = toolkit.get_action('vocabulary_create')(context, data)
        for tag in (u'uk', u'ie', u'de', u'fr', u'es'):
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            toolkit.get_action('tag_create')(context, data)

def quavonz_country_codes(field):
    create_country_codes()
    try:
        tag_list = toolkit.get_action('tag_list')
        country_codes = tag_list(data_dict={'vocabulary_id': 'country_codes'})

#choices:
#- value: bactrian
#  label: Bactrian Camel
#- value: hybrid
#  label: Hybrid Camel


        return country_codes
    except toolkit.ObjectNotFound:
        return None
